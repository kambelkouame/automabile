package com.mbds.assurance.automobile.entites;

import javax.persistence.Id;

public class ResponsabiliteCivil {
    @Id
    public String id;
    public String key;
    public Number tarif;

    public ResponsabiliteCivil() {
        this.key = key;
        this.tarif = tarif;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public Number getTarif() {
        return tarif;
    }
    public void setTarif(Number tarif) {
        this.tarif = tarif;
    }
    
    
}
