package com.mbds.assurance.automobile.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mbds.assurance.automobile.entites.*;
public interface ClientRepository extends MongoRepository<Client, String> {
    
    List<Client> findByNom(String nom);

}
