package com.mbds.assurance.automobile.repositories;
import java.util.List;

import com.mbds.assurance.automobile.entites.*;

import org.springframework.data.mongodb.repository.MongoRepository;
public interface ResponsabiliteCivilRepository extends MongoRepository<ResponsabiliteCivil, String> {

    List<ResponsabiliteCivil> findByKey(String key);
    
}
