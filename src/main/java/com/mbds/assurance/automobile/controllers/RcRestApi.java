package com.mbds.assurance.automobile.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mbds.assurance.automobile.entites.*;
@RestController
@RequestMapping("/api")
public class RcRestApi {

    @PostMapping("/create/tarif")
    public ResponseEntity<List<ResponsabiliteCivil>> importExcelFile(@RequestParam("file") MultipartFile files) throws IOException {
        HttpStatus status = HttpStatus.OK;
        List<ResponsabiliteCivil> responsabiliteCivils = new ArrayList<>();

        XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                ResponsabiliteCivil rCivil = new ResponsabiliteCivil();

                XSSFRow row = worksheet.getRow(index);
                Integer id = (int) row.getCell(0).getNumericCellValue();

                
                rCivil.setKey(row.getCell(0).getStringCellValue());
                rCivil.setTarif(row.getCell(13).getNumericCellValue());
                responsabiliteCivils.add(rCivil);
            }
        }

        return new ResponseEntity<>(responsabiliteCivils, status);
    }
    
}
