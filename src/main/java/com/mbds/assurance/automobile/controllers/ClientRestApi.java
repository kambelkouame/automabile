package com.mbds.assurance.automobile.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import com.mbds.assurance.automobile.entites.Client;
import com.mbds.assurance.automobile.repositories.*;

@RestController
@RequestMapping("/api")
public class ClientRestApi {
    @Autowired
	ClientRepository clientRepository;

    @PostMapping("/insert/client")
	public ResponseEntity<Client> createTutorial(@RequestBody Client client) {
	  try {
		Client _client = clientRepository.save(new Client(client.getNom(), client.getPrenom(),client.getAdresse(),client.getContact(),client.getGenre()));
		return new ResponseEntity<>(_client, HttpStatus.CREATED);
	  } catch (Exception e) {
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	}

    @GetMapping("/get/clients")
  public ResponseEntity<List<Client>> getAllTutorials(@RequestParam(required = false) String nom) {
    try {
      List<Client> clients = new ArrayList<Client>();

      if (nom == null)
        clientRepository.findAll().forEach(clients::add);
      else
      clientRepository.findByNom(nom).forEach(clients::add);

      if (clients.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(clients, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
