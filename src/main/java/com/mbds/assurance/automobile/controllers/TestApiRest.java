package com.mbds.assurance.automobile.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.mbds.assurance.automobile.repositories.*;
@RestController
@RequestMapping("/api")
public class TestApiRest {
    
	@Autowired
	ClientRepository clientRepository;
	
    @GetMapping(path = "/test")
	public String testApiConnection() {
		return "connecting";
	}

	
}
