package com.mbds.assurance.automobile;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes=AutomobileApplicationTests.class)
class AutomobileApplicationTests {

	@Test
	void contextLoads() {
	}

}
